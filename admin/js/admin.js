jQuery(document).ready(function($) {
    $( document ).on( 'change', '#et_pb_event_title, #et_pb_event_location, #et_pb_event_date', function() {
      $('#admin_label').val($('#et_pb_event_title').val() + ' at ' + $('#et_pb_event_location').val() + ' <br> ' + $('#et_pb_event_date').val());
    });
});