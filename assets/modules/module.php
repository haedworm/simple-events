<?php 
class ET_Builder_Module_HW_SE extends ET_Builder_Module {
	function init() {
		$this->name       = esc_html__( 'Event', 'et_builder' );
		$this->slug       = 'et_pb_hw_se';
		$this->fb_support = true;
		$this->main_css_element	= '%%order_class%%.et_pb_hw_se';
		
		$et_accent_color = et_builder_accent_color();

		$this->whitelisted_fields = array(
			'event_title',
			'event_location',
			'show_content',
			'content_new',
			'background_layout',
			'event_date',
			'button_one_text',
			'button_one_url',
			'button_two_text',
			'button_two_url',
			'admin_label',
			'module_id',
			'module_class',
			'background_color',
			'use_background_color',
			'date_background_color',
			'use_date_background_color',
			'use_date_background_image',
			'date_background_image_opacity',
			'date_background_position',
			'date_background_size',
			'date_height',
			'show_time',
			'time_before',
			'time_after',
			'time_prefix',
			'time_suffix',
			'date_inline',
			'day_suffix',
			'show_venue_address',
			'event_address_line_1',
			'event_address_town',
			'event_address_city',
			'event_address_county',
			'event_address_state',
			'event_address_zip',
			'event_address_country',
			'venue_telephone',
			'event_image',
			'event_image_title_text',
			'event_image_alt',
			'button_one_alignment',
			'button_two_alignment',
			'date_box_perspective',
			'perspective_scale',
			// 'date_width',
			'date_image_box_width',
			'date_image_box_width_tablet',
			'date_image_box_width_phone',
			'date_image_box_width_last_edited',
			'content_box_width',
			'content_box_width_tablet',
			'content_box_width_phone',
			'content_box_width_last_edited',
			'details_box_width',
			'details_box_width_tablet',
			'details_box_width_phone',
			'details_box_width_last_edited',
			'date_image_box_order',
			'content_box_order',
			'details_box_order',
		);

		$this->fields_defaults = array(
			'background_layout'   => array( 'light' ),
			'use_background_color' => array( 'on' ),
			'background_color'     => array( '#ffffff', 'add_default_setting' ),
			'use_date_background_color' => array( 'off' ),
			'date_background_color'     => array( $et_accent_color, 'add_default_setting' ),
			'use_date_background_image' => array( 'off' ),
			'date_background_image_opacity' => array( '0.15' ),
			'date_background_position' => array( 'center' ),
			'date_background_size' => array( 'cover' ),
			'show_time'    => array( 'off' ),
			'time_prefix' => array( esc_html__( 'Time', 'et_builder' ) ),
			'date_inline'    => array( 'off' ),
			'day_suffix'    => array( 'off' ),
			'show_venue_address'    => array( 'off' ),
			'admin_label'    => array( '' ),
			'date_height'    => array( '150px' ),
			'button_one_alignment' => array( 'flex-start' ),
			'button_two_alignment' => array( 'flex-start' ),
			'text_orientation'    => array( 'left' ),
			// 'date_width'    => array( '200px' ),
			'date_image_box_width'    => array( '33%' ),
			'content_box_width'    => array( '33%' ),
			'details_box_width'    => array( '33%' ),
			'date_image_box_order'    => array( '1' ),
			'content_box_order'    => array( '2' ),
			'details_box_order'    => array( '3' ),
			'show_content' => array( 'off' ),
		);

		$this->options_toggles = array(
			'advanced' => array(
				'toggles' => array(
					'hw_se_layout' => array(
						'title'    => esc_html__( 'Layout Settings', 'et_builder' ),
						'priority' => 0,
					),
					'title' => array(
						'title'    => esc_html__( 'Main Content Title', 'et_builder' ),
						'priority' => 10,
					),
					'text' => array(
						'title'    => esc_html__( 'Main Content Body', 'et_builder' ),
						'priority' => 20,
						'tabbed_subtoggles' => true,
						'bb_icons_support' => true,
						'sub_toggles' => array(
							'p'     => array(
								'name' => 'P',
								'icon' => 'text-left',
							),
							'a'     => array(
								'name' => 'A',
								'icon' => 'text-link',
							),
							'ul'    => array(
								'name' => 'UL',
								'icon' => 'list',
							),
							'ol'    => array(
								'name' => 'OL',
								'icon' => 'numbered-list',
							),
							'quote' => array(
								'name' => 'QUOTE',
								'icon' => 'text-quote',
							),
						),
					),
					'details_title' => array(
						'title'    => esc_html__( 'Details Box Title', 'et_builder' ),
						'priority' => 30,
					),
					'details_content' => array(
						'title'    => esc_html__( 'Details Box Content', 'et_builder' ),
						'priority' => 30,
					),
				),
			),
			'general'  => array(
				'toggles' => array(
					'main_content' => esc_html__( 'Content', 'et_builder' ),
					'image' => esc_html__( 'Image', 'et_builder' ),
					'link'         => esc_html__( 'Links', 'et_builder' ),
					'elements'     => esc_html__( 'Elements', 'et_builder' ),
					'admin_label'     => esc_html__( 'Admin Label', 'et_builder' ),
				),
			),
			'hw_se_location'  => array(
				'toggles' => array(
					'address'  => esc_html__( 'Address', 'et_builder' ),
				),
			),
			'hw_se_date'  => array(
				'toggles' => array(
					'date'  => esc_html__( 'Date', 'et_builder' ),
					'day'  => esc_html__( 'Day', 'et_builder' ),
					'time' => esc_html__( 'Time', 'et_builder' ),
				),
			),
		);

		$this->advanced_options = array(
			'fonts' => array(
				'title' => array(
					'label'    => esc_html__( 'Title', 'et_builder' ),
					'css'      => array(
						'main' => "{$this->main_css_element} .hw_se_box_two h1, {$this->main_css_element} .hw_se_box_two h2, {$this->main_css_element} .hw_se_box_two h3, {$this->main_css_element} .hw_se_box_two h4, {$this->main_css_element} .hw_se_box_two h5, {$this->main_css_element} .hw_se_box_two h6",
						'line-height' => "{$this->main_css_element} .hw_se_box_two h2, {$this->main_css_element} .hw_se_box_two h2, {$this->main_css_element} .hw_se_box_two h3, {$this->main_css_element} .hw_se_box_two h4, {$this->main_css_element} .hw_se_box_two h5, {$this->main_css_element} .hw_se_box_two h6",
						'important' => 'all',
					),
					'font_size' => array(
						'default'      => '30px',
					),
					'line_height' => array(
						'default' => '1.6em',
					),
					'header_level' => array(
						'default' => 'h4',
					),
				),
				'text'   => array(
					'label'    => esc_html__( 'Main Content', 'et_builder' ),
					'css'      => array(
						'line_height' => "{$this->main_css_element} p",
						'color' => "{$this->main_css_element}.hw_se_content",
					),
					'line_height' => array(
						'default' => floatval( et_get_option( 'body_font_height', '1.7' ) ) . 'em',
					),
					'font_size' => array(
						'default' => absint( et_get_option( 'body_font_size', '14' ) ) . 'px',
					),
					'toggle_slug' => 'text',
					'sub_toggle'  => 'p',
					'hide_text_align' => false,
					'hide_text_shadow' => true,
				),
				'details_title' => array(
					'label'    => esc_html__( 'Details Title', 'et_builder' ),
					'css'      => array(
						'main' => "{$this->main_css_element} .hw_se_box_three h2",
						'line-height' => "{$this->main_css_element} .hw_se_box_three h2",
					),
					'font_size' => array(
						'default'      => '30px',
					),
					'line_height' => array(
						'default' => '1.6em',
					),
				),
				'details_content' => array(
					'label'    => esc_html__( 'Details Content', 'et_builder' ),
					'css'      => array(
						'main' => "{$this->main_css_element} .hw_se_box_three",
					),
					'line_height' => array(
						'default' => floatval( et_get_option( 'body_font_height', '1.7' ) ) . 'em',
					),
					'font_size' => array(
						'default' => '13px',
					),
				),
				'link'   => array(
					'label'    => esc_html__( 'Link', 'et_builder' ),
					'css'      => array(
						'main' => "{$this->main_css_element} a",
						'color' => "{$this->main_css_element}.hw_se_content a",
					),
					'line_height' => array(
						'default' => '1em',
					),
					'font_size' => array(
						'default' => absint( et_get_option( 'body_font_size', '14' ) ) . 'px',
					),
					'toggle_slug' => 'text',
					'sub_toggle'  => 'a',
				),
				'ul'   => array(
					'label'    => esc_html__( 'Unordered List', 'et_builder' ),
					'css'      => array(
						'main'        => "{$this->main_css_element} ul",
						'color'       => "{$this->main_css_element}.hw_se_content ul",
						'line_height' => "{$this->main_css_element} ul li",
					),
					'line_height' => array(
						'default' => '1em',
					),
					'font_size' => array(
						'default' => '14px',
					),
					'toggle_slug' => 'text',
					'sub_toggle'  => 'ul',
				),
				'ol'   => array(
					'label'    => esc_html__( 'Ordered List', 'et_builder' ),
					'css'      => array(
						'main'        => "{$this->main_css_element} ol",
						'color'       => "{$this->main_css_element}.hw_se_content ol",
						'line_height' => "{$this->main_css_element} ol li",
					),
					'line_height' => array(
						'default' => '1em',
					),
					'font_size' => array(
						'default' => '14px',
					),
					'toggle_slug' => 'text',
					'sub_toggle'  => 'ol',
				),
				'quote'   => array(
					'label'    => esc_html__( 'Blockquote', 'et_builder' ),
					'css'      => array(
						'main' => "{$this->main_css_element} blockquote",
						'color' => "{$this->main_css_element}.hw_se_content blockquote",
					),
					'line_height' => array(
						'default' => '1em',
					),
					'font_size' => array(
						'default' => '14px',
					),
					'toggle_slug' => 'text',
					'sub_toggle'  => 'quote',
				),
				'hw_se_date_day' => array(
					'label'    => esc_html__( 'Day', 'et_builder' ),
					'css'      => array(
						'main' => "{$this->main_css_element} .hw_se_day",
						'line-height' => "{$this->main_css_element} .hw_se_day",
					),
					'font_size' => array(
						'default'      => '30px',
					),
					'line_height' => array(
						'default' => '1.2em',
					),
				),
				'hw_se_date_month' => array(
					'label'    => esc_html__( 'Month', 'et_builder' ),
					'css'      => array(
						'main' => "{$this->main_css_element} .hw_se_month",
						'line-height' => "{$this->main_css_element} .hw_se_month",
					),
					'font_size' => array(
						'default'      => '30px',
					),
					'line_height' => array(
						'default' => '1.2em',
					),
				),
				'hw_se_date_year' => array(
					'label'    => esc_html__( 'Year', 'et_builder' ),
					'css'      => array(
						'main' => "{$this->main_css_element} .hw_se_year",
						'line-height' => "{$this->main_css_element} .hw_se_year",
					),
					'font_size' => array(
						'default'      => '15px',
					),
					'line_height' => array(
						'default' => '1.2em',
					),
				),
			),
			'background' => array(
				'use_background_color'          => false,
				'use_background_color_gradient' => true,
				'settings'                      => array(
					'color' => 'alpha',
				),
			),
			'button' => array(
				'button' => array(
					'label' => esc_html__( 'Button', 'et_builder' ),
					'css'      => array(
						'main' => "{$this->main_css_element} .et_pb_button",
						'plugin_main' => "{$this->main_css_element} .hw_se_buttons .et_pb_button",
					),
				),
				'button_two' => array(
					'label' => esc_html__( 'Button Two', 'et_builder' ),
					'css'      => array(
						'main' => "{$this->main_css_element} .read_more.et_pb_button",
						'plugin_main' => "{$this->main_css_element} .hw_se_buttons .read_more.et_pb_button",
					),
				),
			),
		);

		$this->custom_css_options = array(
			'custom_day' => array(
				'label'    => esc_html__( 'Event Day', 'et_builder' ),
				'selector' => '.hw_se_day',
			),
			'custom_month' => array(
				'label'    => esc_html__( 'Event Month', 'et_builder' ),
				'selector' => '.hw_se_month',
			),
			'custom_year' => array(
				'label'    => esc_html__( 'Event Year', 'et_builder' ),
				'selector' => '.hw_se_year',
			),
			'custom_time' => array(
				'label'    => esc_html__( 'Event Time', 'et_builder' ),
				'selector' => '.hw_se_time',
			),
		);
	}

	function get_fields() {
		$fields = array(
			'event_title' => array(
				'label'           => esc_html__( 'Artist/Speaker', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Enter an artist, performer or speaker name.', 'et_builder' ),
				'toggle_slug'     => 'main_content',
			),
			'event_location' => array(
				'label'           => esc_html__( 'Venue Name', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Enter the name of the venue or event.', 'et_builder' ),
				'toggle_slug'     => 'main_content',
			),
			'show_content' => array(
				'label'             => esc_html__( 'Content Toggle', 'et_builder' ),
				'type'              => 'yes_no_button',
				'option_category'   => 'layout',
				'options'           => array(
					'off' => esc_html__( 'Off', 'et_builder' ),
					'on'  => esc_html__( 'On', 'et_builder' ),
				),
				'affects'           => array(
					'content_new',
				),
				'description'       => esc_html__( 'Enable to add Content to the main content area, disable if you only want to show the title.', 'et_builder' ),
				'toggle_slug'       => 'main_content',
			),
			'content_new' => array(
				'label'           => esc_html__( 'Event Description', 'et_builder' ),
				'type'            => 'tiny_mce',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Enter a description for your event.', 'et_builder' ),
				'depends_show_if'   => 'on',
				'toggle_slug'     => 'main_content',
			),
			'background_layout' => array(
				'label'             => esc_html__( 'Text Color', 'et_builder' ),
				'type'              => 'select',
				'option_category'   => 'configuration',
				'options'           => array(
					'light' => esc_html__( 'Dark', 'et_builder' ),
					'dark'  => esc_html__( 'Light', 'et_builder' ),
				),
				'tab_slug'          => 'advanced',
				'toggle_slug'       => 'text',
				'sub_toggle'        => 'p',
				'description'       => esc_html__( 'Here you can choose the value of your text. If you are working with a dark background, then your text should be set to light. If you are working with a light background, then your text should be dark.', 'et_builder' ),
			),
			'background_color' => array(
				'label'             => esc_html__( 'Background Color', 'et_builder' ),
				'type'              => 'color-alpha',
				'description'       => esc_html__( 'Here you can define a custom background color for your CTA.', 'et_builder' ),
				'depends_default'   => true,
				'toggle_slug'       => 'background',
			),
			'use_background_color' => array(
				'label'           => esc_html__( 'Use Background Color', 'et_builder' ),
				'type'            => 'yes_no_button',
				'option_category' => 'configuration',
				'options'         => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'affects'           => array(
					'background_color',
				),
				'toggle_slug'     => 'background',
				'description'     => esc_html__( 'Here you can choose whether background color setting below should be used or not.', 'et_builder' ),
			),
// DATE FORMATTING SETTINGS		
			'date_inline' => array(
				'label'           => esc_html__( 'Wrap Date Items', 'et_builder' ),
				'type'            => 'yes_no_button',
				'option_category' => 'configuration',
				'options'         => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'default'         => 'off',
				'description'     => esc_html__( 'Show the Day and Month on separate lines.', 'et_builder' ),
				'tab_slug'    			=> 'hw_se_date',
				'toggle_slug'       	=> 'date',
			),
			'day_suffix' => array(
				'label'           => esc_html__( 'Add Day Suffix', 'et_builder' ),
				'type'            => 'yes_no_button',
				'option_category' => 'configuration',
				'options'         => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'default'         => 'off',
				'description'     => esc_html__( 'Add Suffix to Day field i.e. 1st, 2nd, 3rd, 4th.', 'et_builder' ),
				'tab_slug'    			=> 'hw_se_date',
				'toggle_slug'       	=> 'day',
			),
/*			'date_width' => array(
				'label'           => esc_html__( 'Date Box Width', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'default'           => '200px',
				'description'     => esc_html__( 'Set a width for the Date Box. Default is 200px.', 'et_builder' ),
				'tab_slug'    			=> 'hw_se_layout',
				'toggle_slug'       	=> 'date',
			),
*/
			'date_image_box_width' => array(
				'label'           => esc_html__( 'Date/Image Width', 'et_builder' ),
				'type'            => 'range',
				'option_category' => 'configuration',
				'mobile_options'  => true,
				'default'           => '33%',
				'range_settings'  => array(
						'min'  => '0',
						'max'  => '100',
						'step' => '1',						
				),
				'description'	=> esc_html__( 'Set a width for the Date/Image Column.', 'et_builder' ),
				'tab_slug'		=> 'advanced',
				'toggle_slug'	=> 'hw_se_layout',
			),
			'date_image_box_order' => array(
				'label'             => esc_html__( 'Order', 'et_builder' ),
				'type'              => 'select',
				'option_category'   => 'configuration',
				'options'           => array(
					'1'	=> esc_html__( 'First', 'et_builder' ),
					'2'	=> esc_html__( 'Second', 'et_builder' ),
					'3'	=> esc_html__( 'Third', 'et_builder' ),
				),
				'default'           => '1',
				'tab_slug'		=> 'advanced',
				'toggle_slug'	=> 'hw_se_layout',
				'description'       => esc_html__( 'Set the Flex Order of the Date/Image Column.', 'et_builder' ),
			),
			'content_box_width' => array(
				'label'           => esc_html__( 'Main Content Width', 'et_builder' ),
				'type'            => 'range',
				'option_category' => 'configuration',
				'mobile_options'  => true,
				'default'           => '33%',
				'range_settings'  => array(
						'min'  => '0',
						'max'  => '100',
						'step' => '1',						
				),
				'description'     => esc_html__( 'Set a width for the Main Content Column.', 'et_builder' ),
				'tab_slug'		=> 'advanced',
				'toggle_slug'	=> 'hw_se_layout',
			),
			'content_box_order' => array(
				'label'             => esc_html__( 'Order', 'et_builder' ),
				'type'              => 'select',
				'option_category'   => 'configuration',
				'options'           => array(
					'1'	=> esc_html__( 'First', 'et_builder' ),
					'2'	=> esc_html__( 'Second', 'et_builder' ),
					'3'	=> esc_html__( 'Third', 'et_builder' ),
				),
				'default'           => '2',
				'tab_slug'		=> 'advanced',
				'toggle_slug'	=> 'hw_se_layout',
				'description'       => esc_html__( 'Set the Flex Order of the Main Content Column.', 'et_builder' ),
			),
			'details_box_width' => array(
				'label'           => esc_html__( 'Details Width', 'et_builder' ),
				'type'            => 'range',
				'option_category' => 'configuration',
				'mobile_options'  => true,
				'default'           => '33%',
				'range_settings'  => array(
						'min'  => '0',
						'max'  => '100',
						'step' => '1',						
				),
				'description'	=> esc_html__( 'Set a width for the Details Column.', 'et_builder' ),
				'tab_slug'		=> 'advanced',
				'toggle_slug'	=> 'hw_se_layout',
			),
			'details_box_order' => array(
				'label'             => esc_html__( 'Order', 'et_builder' ),
				'type'              => 'select',
				'option_category'   => 'configuration',
				'options'           => array(
					'1'	=> esc_html__( 'First', 'et_builder' ),
					'2'	=> esc_html__( 'Second', 'et_builder' ),
					'3'	=> esc_html__( 'Third', 'et_builder' ),
				),
				'default'           => '3',
				'tab_slug'		=> 'advanced',
				'toggle_slug'	=> 'hw_se_layout',
				'description'       => esc_html__( 'Set the Flex Order of the Details Column.', 'et_builder' ),
			),
// RESPONSIVE
			'date_image_box_width_tablet' => array(
				'type'        => 'skip',
				'tab_slug'    => 'advanced',
				'toggle_slug' => 'hw_se_layout',
			),
			'date_image_box_width_phone' => array(
				'type'        => 'skip',
				'tab_slug'    => 'advanced',
				'toggle_slug' => 'hw_se_layout',
			),
			'date_image_box_width_last_edited' => array(
				'type'        => 'skip',
				'tab_slug'    => 'advanced',
				'toggle_slug' => 'hw_se_layout',
			),
			'content_box_width_tablet' => array(
				'type'        => 'skip',
				'tab_slug'    => 'advanced',
				'toggle_slug' => 'hw_se_layout',
			),
			'content_box_width_phone' => array(
				'type'        => 'skip',
				'tab_slug'    => 'advanced',
				'toggle_slug' => 'hw_se_layout',
			),
			'content_box_width_last_edited' => array(
				'type'        => 'skip',
				'tab_slug'    => 'advanced',
				'toggle_slug' => 'hw_se_layout',
			),
			'details_box_width_tablet' => array(
				'type'        => 'skip',
				'tab_slug'    => 'advanced',
				'toggle_slug' => 'hw_se_layout',
			),
			'details_box_width_phone' => array(
				'type'        => 'skip',
				'tab_slug'    => 'advanced',
				'toggle_slug' => 'hw_se_layout',
			),
			'details_box_width_last_edited' => array(
				'type'        => 'skip',
				'tab_slug'    => 'advanced',
				'toggle_slug' => 'hw_se_layout',
			),

			'date_height' => array(
				'label'           => esc_html__( 'Date Box Height', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'default'			=> '150px',
				'description'     => esc_html__( 'Set a height for the Date Box, this will also be the height of the module in full width or a single column layout. Default is 150px.', 'et_builder' ),
				'tab_slug'    			=> 'hw_se_date',
				'toggle_slug'       	=> 'date',
			),
			'date_background_color' => array(
				'label'           => esc_html__( 'Date Box Background', 'et_builder' ),
				'type'            => 'color-alpha',
				'depends_show_if' => 'on',
				'description'     => esc_html__( 'Define a custom background color for the date box. Solid colors work best here.', 'et_builder' ),
				'additional_code' => '<span class="et-pb-reset-setting reset-default-color" style="display: none;"></span>',
				'tab_slug'    			=> 'hw_se_date',
				'toggle_slug'       	=> 'date',
			),
			'use_date_background_color' => array(
				'label'             => esc_html__( 'Use Date Background', 'et_builder' ),
				'type'              => 'yes_no_button',
				'option_category'   => 'color_option',
				'options'           => array(
					'off' => esc_html__( 'No', 'et_builder' ),
					'on'  => esc_html__( 'Yes', 'et_builder' ),
				),
				'default'           => 'off',
				'affects'           => array(
					'date_background_color',
				),
				'description'       => esc_html__( 'Enabling this option will remove the background color of the Date Box.', 'et_builder' ),
				'tab_slug'    			=> 'hw_se_date',
				'toggle_slug'       	=> 'date',
			),
			'use_date_background_image' => array(
				'label'             => esc_html__( 'Use Background Image', 'et_builder' ),
				'type'              => 'yes_no_button',
				'option_category'   => 'color_option',
				'options'           => array(
					'off' => esc_html__( 'No', 'et_builder' ),
					'on'  => esc_html__( 'Yes', 'et_builder' ),
				),
				'default'           => 'off',
				'affects'           => array(
					'date_background_image_opacity',
					'date_background_position',
					'date_background_size',
				),
				'description'       => esc_html__( 'Enabling this option will add the event image to the background of the date box.', 'et_builder' ),
				'tab_slug'    			=> 'hw_se_date',
				'toggle_slug'       	=> 'date',
			),
			'date_background_image_opacity' => array(
				'label'             => esc_html__( 'Background Image Opacity', 'et_builder' ),
				'type'              => 'range',
				'option_category'   => 'configuration',
				'range_settings'  => array(
						'min'  => '0',
						'max'  => '1',
						'step' => '0.01',						
				),
				'default'           => '0.15',
				'depends_show_if'   => 'on',
				'description'       => esc_html__( 'Lower the image opacity of the date box background image if you want the background color to show through.', 'et_builder' ),
				'tab_slug'    			=> 'hw_se_date',
				'toggle_slug'       	=> 'date',
			),
			'date_background_position' => array(
				'label'           => esc_html__( 'Background Image Position', 'et_builder' ),
				'type'            => 'select',
				'option_category' => 'layout',
				'options' => array(
					'top_left'      => esc_html__( 'Top Left', 'et_builder' ),
					'top_center'    => esc_html__( 'Top Center', 'et_builder' ),
					'top_right'     => esc_html__( 'Top Right', 'et_builder' ),
					'center_left'   => esc_html__( 'Center Left', 'et_builder' ),
					'center'        => esc_html__( 'Center', 'et_builder' ),
					'center_right'  => esc_html__( 'Center Right', 'et_builder' ),
					'bottom_left'   => esc_html__( 'Bottom Left', 'et_builder' ),
					'bottom_center' => esc_html__( 'Bottom Center', 'et_builder' ),
					'bottom_right'  => esc_html__( 'Bottom Right', 'et_builder' ),
				),
				'default'           => 'center',
				'depends_show_if'   => 'on',
				'tab_slug'    			=> 'hw_se_date',
				'toggle_slug'       	=> 'date',
			),
			'date_background_size' => array(
				'label'           => esc_html__( 'Background Image Size', 'et_builder' ),
				'type'            => 'select',
				'option_category' => 'layout',
				'options'         => array(
					'cover'   => esc_html__( 'Cover', 'et_builder' ),
					'contain' => esc_html__( 'Fit', 'et_builder' ),
					'initial' => esc_html__( 'Actual Size', 'et_builder' ),
				),
				'default'           => 'cover',
				'depends_show_if'   => 'on',
				'tab_slug'    			=> 'hw_se_date',
				'toggle_slug'       	=> 'date',
			),
// DATE FORMATTING SETTINGS		
// TIME FORMATTING SETTINGS		
			'show_time' => array(
				'label'           => esc_html__( 'Show Time', 'et_builder' ),
				'type'            => 'yes_no_button',
				'option_category' => 'configuration',
				'options'         => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'affects'           => array(
					'time_before',
					'time_prefix',
					'time_suffix',
					'time_after',
				),
				'tab_slug'    			=> 'hw_se_date',
				'toggle_slug'       	=> 'time',
				'description'     => esc_html__( 'Here you can choose whether to display the event time.', 'et_builder' ),
			),
			'time_before' => array(
				'label'           => esc_html__( 'Extra Field Before Time', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'depends_show_if' => 'on',
				'default'           => '',
				'description'     => esc_html__( 'Use this to show more details before the time field. i.e Doors Open: 20:00', 'et_builder' ),
				'tab_slug'    			=> 'hw_se_date',
				'toggle_slug'       	=> 'time',
			),
			'time_prefix' => array(
				'label'           => esc_html__( 'Time Prefix', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'depends_show_if' => 'on',
				'default'           => 'Start Time',
				'description'     => esc_html__( 'Text before timestamp. You can also add other times here i.e Doors Open: 20:00 | Start Time: ', 'et_builder' ),
				'tab_slug'    			=> 'hw_se_date',
				'toggle_slug'       	=> 'time',
			),
			'time_suffix' => array(
				'label'           => esc_html__( 'Time Suffix', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'depends_show_if' => 'on',
				'default'           => '',
				'description'     => esc_html__( 'Text after timestamp. You can also add other times here i.e GMT | End Time: 02:00 GMT', 'et_builder' ),
				'tab_slug'    			=> 'hw_se_date',
				'toggle_slug'       	=> 'time',
			),
			'time_after' => array(
				'label'           => esc_html__( 'Extra Field After Time', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'depends_show_if' => 'on',
				'default'           => '',
				'description'     => esc_html__( 'Use this to show more details after the time field. i.e End Time: 02:00', 'et_builder' ),
				'tab_slug'    			=> 'hw_se_date',
				'toggle_slug'       	=> 'time',
			),
// TIME FORMATTING SETTINGS		
			'show_venue_address' => array(
				'label'           => esc_html__( 'Show Venue Address', 'et_builder' ),
				'type'            => 'yes_no_button',
				'option_category' => 'configuration',
				'options'         => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'affects'           => array(
					'event_address_line_1',
					'event_address_town',
					'event_address_city',
					'event_address_county',
					'event_address_state',
					'event_address_zip',
					'event_address_country',
					'venue_telephone',
				),
				'tab_slug'    			=> 'hw_se_location',
				'toggle_slug'       	=> 'address',
				'description'     => esc_html__( 'Here you can choose whether to show the venue address details.', 'et_builder' ),
			),
			'event_address_line_1' => array(
				'label'           => esc_html__( 'Line One', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Enter a Street Name for the event Venue.', 'et_builder' ),
				'tab_slug'    			=> 'hw_se_location',
				'toggle_slug'       	=> 'address',
			),
			'event_address_town' => array(
				'label'           => esc_html__( 'Town', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Enter a Town for the event Venue if required.', 'et_builder' ),
				'tab_slug'    			=> 'hw_se_location',
				'toggle_slug'       	=> 'address',
			),
			'event_address_city' => array(
				'label'           => esc_html__( 'City', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Enter a City for the event Venue if required.', 'et_builder' ),
				'tab_slug'    			=> 'hw_se_location',
				'toggle_slug'       	=> 'address',
			),
			'event_address_county' => array(
				'label'           => esc_html__( 'County', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Enter a County for the event Venue if required.', 'et_builder' ),
				'tab_slug'    			=> 'hw_se_location',
				'toggle_slug'       	=> 'address',
			),
			'event_address_state' => array(
				'label'           => esc_html__( 'State/Region', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Enter a State or Region for the event Venue if required.', 'et_builder' ),
				'tab_slug'    			=> 'hw_se_location',
				'toggle_slug'       	=> 'address',
			),
			'event_address_zip' => array(
				'label'           => esc_html__( 'Zip/Post Code', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Enter a Zip or Post Code for the event Venue if required. This field is used to generate a google map link.', 'et_builder' ),
				'tab_slug'    			=> 'hw_se_location',
				'toggle_slug'       	=> 'address',
			),
			'event_address_country' => array(
				'label'           => esc_html__( 'Country', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Enter a Country for the event Venue if required.', 'et_builder' ),
				'tab_slug'    			=> 'hw_se_location',
				'toggle_slug'       	=> 'address',
			),
			'venue_telephone' => array(
				'label'           => esc_html__( 'Venue Telephone', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Enter a Telephone Number for the event Venue if required.', 'et_builder' ),
				'tab_slug'    			=> 'hw_se_location',
				'toggle_slug'       	=> 'address',
			),
			'event_image' => array(
				'label'              => esc_html__( 'Event Image URL', 'et_builder' ),
				'type'               => 'upload',
				'option_category'    => 'basic_option',
				'upload_button_text' => esc_attr__( 'Upload an image', 'et_builder' ),
				'choose_text'        => esc_attr__( 'Choose an Image', 'et_builder' ),
				'update_text'        => esc_attr__( 'Set As Image', 'et_builder' ),
				'affects'            => array(
					'event_image_alt',
					'event_image_title_text',
				),
				'description'        => esc_html__( 'Upload your desired image, or type in the URL to the image you would like to display.', 'et_builder' ),
				'toggle_slug'        => 'image',
			),
			'event_date' => array(
				'label'           => esc_html__( 'Event Date', 'et_builder' ),
				'type'            => 'date_picker',
				'option_category' => 'basic_option',
				'description'     => et_get_safe_localization( sprintf( __( 'This is the date and time of your event which is based on your timezone settings in your <a href="%1$s" target="_blank" title="WordPress General Settings">WordPress General Settings</a>', 'et_builder' ), esc_url( admin_url( 'options-general.php' ) ) ) ),
				'toggle_slug'     => 'main_content',
			),
			'event_image_alt' => array(
				'label'           => esc_html__( 'Event Image Alternative Text', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'depends_default' => true,
				'depends_to'      => array(
					'event_image',
				),
				'description'     => esc_html__( 'This defines the HTML ALT text. A short description of your event image can be placed here.', 'et_builder' ),
				'toggle_slug'     => 'image',
			),
			'event_image_title_text' => array(
				'label'           => esc_html__( 'Event Image Title Text', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'depends_default' => true,
				'depends_to'      => array(
					'event_image',
				),
				'description'     => esc_html__( 'This defines the HTML Title text for your event image.', 'et_builder' ),
				'toggle_slug'     => 'image',
			),
			'button_one_url' => array(
				'label'           => esc_html__( 'Button One URL', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Input the destination URL for button one.', 'et_builder' ),
				'toggle_slug'     => 'link',
			),
			'button_one_text' => array(
				'label'           => esc_html__( 'Button One Text', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Enter the text used for button one.', 'et_builder' ),
				'toggle_slug'     => 'main_content',
			),
			'button_one_alignment' => array(
				'label'           => esc_html__( 'Button One Alignment', 'et_builder' ),
				'type'            => 'select',
				'option_category' => 'configuration',
				'options'         => array(
					'flex-start'  => esc_html__( 'Left', 'et_builder' ),
					'center' => esc_html__( 'Center', 'et_builder' ),
					'flex-end' => esc_html__( 'Right', 'et_builder' ),
				),
				'toggle_slug'     => 'main_content',
				'description'     => esc_html__( 'Change the alignment for button one.', 'et_builder' ),
			),
			'button_two_url' => array(
				'label'           => esc_html__( 'Button Two URL', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Input the destination URL for button two.', 'et_builder' ),
				'toggle_slug'     => 'link',
			),
			'button_two_text' => array(
				'label'           => esc_html__( 'Button Two Text', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Adjust the text used from the signup button.', 'et_builder' ),
				'toggle_slug'     => 'main_content',
			),
			'button_two_alignment' => array(
				'label'           => esc_html__( 'Button Two Alignment', 'et_builder' ),
				'type'            => 'select',
				'option_category' => 'configuration',
				'options'         => array(
					'flex-start'  => esc_html__( 'Left', 'et_builder' ),
					'center' => esc_html__( 'Center', 'et_builder' ),
					'flex-end' => esc_html__( 'Right', 'et_builder' ),
				),
				'toggle_slug'     => 'main_content',
				'description'     => esc_html__( 'Enter the text used for button two.', 'et_builder' ),
			),
			'admin_label' => array(
				'label'       => esc_html__( 'Admin Label', 'et_builder' ),
				'type'        => 'text',
				'description' => esc_html__( 'This will change the label of the module in the builder for easy identification.', 'et_builder' ),
				'toggle_slug' => 'admin_label',
			),
			'module_id' => array(
				'label'           => esc_html__( 'CSS ID', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'configuration',
				'tab_slug'        => 'custom_css',
				'toggle_slug'     => 'classes',
				'option_class'    => 'et_pb_custom_css_regular',
			),
			'module_class' => array(
				'label'           => esc_html__( 'CSS Class', 'et_builder' ),
				'type'            => 'text',
				'option_category' => 'configuration',
				'tab_slug'        => 'custom_css',
				'toggle_slug'     => 'classes',
				'option_class'    => 'et_pb_custom_css_regular',
			),
		);
		return $fields;
	}

	function shortcode_callback( $atts, $content = null, $function_name ) {

		$module_id             = $this->shortcode_atts['module_id'];
		$module_class          = $this->shortcode_atts['module_class'];
		$event_title         = $this->shortcode_atts['event_title'];
		$show_content         = $this->shortcode_atts['show_content'];
		$header_level          = $this->shortcode_atts['title_level'];
		$event_location      = $this->shortcode_atts['event_location'];
		$event_date           = $this->shortcode_atts['event_date'];
		$button_one_url    = $this->shortcode_atts['button_one_url'];
		$button_one_text   = $this->shortcode_atts['button_one_text'];
		$button_two_url    = $this->shortcode_atts['button_two_url'];
		$button_two_text   = $this->shortcode_atts['button_two_text'];
		
		$ul_type              = $this->shortcode_atts['ul_type'];
		$ul_position          = $this->shortcode_atts['ul_position'];
		$ul_item_indent       = $this->shortcode_atts['ul_item_indent'];
		$ol_type              = $this->shortcode_atts['ol_type'];
		$ol_position          = $this->shortcode_atts['ol_position'];
		$ol_item_indent       = $this->shortcode_atts['ol_item_indent'];
		$background_layout    = $this->shortcode_atts['background_layout'];
		
		$button_custom = $this->shortcode_atts['custom_button'];
		$custom_icon   = $this->shortcode_atts['button_icon'];

		$button_one_icon                = $this->shortcode_atts['button_icon'];
		$custom_button_one              = $this->shortcode_atts['custom_button_one'];
		$button_two_icon                = $this->shortcode_atts['button_two_icon'];
		$custom_button_two              = $this->shortcode_atts['custom_button_two'];

		$background_color        = $this->shortcode_atts['background_color'];
		$use_background_color  = $this->shortcode_atts['use_background_color'];
		$date_background_color        = $this->shortcode_atts['date_background_color'];
		$use_date_background_color  = $this->shortcode_atts['use_date_background_color'];
		$use_date_background_image  = $this->shortcode_atts['use_date_background_image'];
		$date_background_image_opacity  = $this->shortcode_atts['date_background_image_opacity'];
		$date_background_position  = $this->shortcode_atts['date_background_position'];
		$date_background_size  = $this->shortcode_atts['date_background_size'];

		$date_height        = $this->shortcode_atts['date_height'];
		$time_background_color        = $this->shortcode_atts['time_background_color'];
		$use_time_background_color  = $this->shortcode_atts['use_time_background_color'];
		$show_time  = $this->shortcode_atts['show_time'];
		$time_before  = $this->shortcode_atts['time_before'];
		$time_after  = $this->shortcode_atts['time_after'];
		$time_prefix  = $this->shortcode_atts['time_prefix'];
		$time_suffix  = $this->shortcode_atts['time_suffix'];
		$date_inline  = $this->shortcode_atts['date_inline'];
		$day_suffix  = $this->shortcode_atts['day_suffix'];
		$show_venue_address  = $this->shortcode_atts['show_venue_address'];
		$event_address_line_1  = $this->shortcode_atts['event_address_line_1'];
		$event_address_town  = $this->shortcode_atts['event_address_town'];
		$event_address_city  = $this->shortcode_atts['event_address_city'];
		$event_address_county  = $this->shortcode_atts['event_address_county'];
		$event_address_state  = $this->shortcode_atts['event_address_state'];
		$event_address_zip  = $this->shortcode_atts['event_address_zip'];
		$event_address_country  = $this->shortcode_atts['event_address_country'];
		$venue_telephone  = $this->shortcode_atts['venue_telephone'];
		$event_image                 = $this->shortcode_atts['event_image'];
		$event_image_alt                     = $this->shortcode_atts['event_image_alt'];
		$event_image_title_text              = $this->shortcode_atts['event_image_title_text'];

		$button_one_alignment = $this->shortcode_atts['button_one_alignment'];
		$button_two_alignment = $this->shortcode_atts['button_two_alignment'];

		// $date_width        = $this->shortcode_atts['date_width'];
		$date_image_box_width	= $this->shortcode_atts['date_image_box_width'];
		$content_box_width		= $this->shortcode_atts['content_box_width'];
		$details_box_width		= $this->shortcode_atts['details_box_width'];

		$date_image_box_order	= $this->shortcode_atts['date_image_box_order'];
		$content_box_order		= $this->shortcode_atts['content_box_order'];
		$details_box_order		= $this->shortcode_atts['details_box_order'];

		$date_image_box_width_tablet      = $this->shortcode_atts['date_image_box_width_tablet'];
		$date_image_box_width_phone       = $this->shortcode_atts['date_image_box_width_phone'];
		$date_image_box_width_last_edited = $this->shortcode_atts['date_image_box_width_last_edited'];
		$content_box_width_tablet      = $this->shortcode_atts['content_box_width_tablet'];
		$content_box_width_phone       = $this->shortcode_atts['content_box_width_phone'];
		$content_box_width_last_edited = $this->shortcode_atts['content_box_width_last_edited'];
		$details_box_width_tablet      = $this->shortcode_atts['details_box_width_tablet'];
		$details_box_width_phone       = $this->shortcode_atts['details_box_width_phone'];
		$details_box_width_last_edited = $this->shortcode_atts['details_box_width_last_edited'];

		$module_class = ET_Builder_Element::add_module_order_class( $module_class, $function_name );
		$this->shortcode_content = et_builder_replace_code_content_entities( $this->shortcode_content );

		$class = " et_pb_bg_layout_{$background_layout}{$this->get_text_orientation_classname()}";

		$time = date( 'H:i', strtotime( $event_date ) );
		$day = date( 'j', strtotime( $event_date ) );
		$suffix = date( 'S', strtotime( $event_date ) );
		$month = date( 'M', strtotime( $event_date ) );
		$month_num = date( 'm', strtotime( $event_date ) );
		$year = date( 'Y', strtotime( $event_date ) );

		$button_one_url = trim( $button_one_url );
		$button_two_url = trim( $button_two_url );
		
		if ( 'on' !== $show_content ) {
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .hw_se_box_two',
				'declaration' => 'display: flex;align-items: center;'
			) );
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .hw_se_box_two .header',
				'declaration' => 'width: 100%'
			) );
		}

		if ( '' !== $ul_type || '' !== $ul_position || '' !== $ul_item_indent ) {
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% ul',
				'declaration' => sprintf(
					'%1$s
					%2$s
					%3$s',
					'' !== $ul_type ? sprintf( 'list-style-type: %1$s;', esc_html( $ul_type ) ) : '',
					'' !== $ul_position ? sprintf( 'list-style-position: %1$s;', esc_html( $ul_position ) ) : '',
					'' !== $ul_item_indent ? sprintf( 'padding-left: %1$s;', esc_html( $ul_item_indent ) ) : ''
				),
			) );
		}

		if ( '' !== $ol_type || '' !== $ol_position || '' !== $ol_item_indent ) {
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% ol',
				'declaration' => sprintf(
					'%1$s
					%2$s
					%3$s',
					'' !== $ol_type ? sprintf( 'list-style-type: %1$s;', esc_html( $ol_type ) ) : '',
					'' !== $ol_position ? sprintf( 'list-style-position: %1$s;', esc_html( $ol_position ) ) : '',
					'' !== $ol_item_indent ? sprintf( 'padding-left: %1$s;', esc_html( $ol_item_indent ) ) : ''
				),
			) );
		}

		if ( '' !== $event_title ) {
			$event_title = sprintf( '<%1$s class="header" itemprop="name"><span class="performer" itemprop="performer">%2$s</span> at %3$s</%1$s>', et_pb_process_header_level( $header_level, 'h4' ), $event_title, $event_location );
		}		
			
		if ( '' !== $button_one_alignment ) {
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .hw_button_one.et_pb_button',
				'declaration' => sprintf(
					'justify-content: %1$s;',
					esc_html( $button_one_alignment )
				),
			) );
		}
		if ( '' !== $button_two_alignment ) {
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .hw_button_two.et_pb_button',
				'declaration' => sprintf(
					'justify-content: %1$s;',
					esc_html( $button_two_alignment )
				),
			) );
		} 
		if ( '' !== $date_image_box_width_tablet || '' !== $date_image_box_width_phone || '' !== $date_image_box_width ) {
			$date_image_box_width_responsive_active = et_pb_get_responsive_status( $date_image_box_width_last_edited );

			$date_image_box_width_values = array(
				'desktop' => $date_image_box_width,
				'tablet'  => $date_image_box_width_responsive_active ? $date_image_box_width_tablet : '',
				'phone'   => $date_image_box_width_responsive_active ? $date_image_box_width_phone : '',
			);

			et_pb_generate_responsive_css( $date_image_box_width_values, '%%order_class%% .hw_se_box_one', 'flex-basis', $function_name );
		}
		if ( '' !== $content_box_width_tablet || '' !== $content_box_width_phone || '' !== $content_box_width ) {
			$content_box_width_responsive_active = et_pb_get_responsive_status( $content_box_width_last_edited );

			$content_box_width_values = array(
				'desktop' => $content_box_width,
				'tablet'  => $content_box_width_responsive_active ? $content_box_width_tablet : '',
				'phone'   => $content_box_width_responsive_active ? $content_box_width_phone : '',
			);

			et_pb_generate_responsive_css( $content_box_width_values, '%%order_class%% .hw_se_box_two', 'flex-basis', $function_name );
		}
		if ( '' !== $details_box_width_tablet || '' !== $details_box_width_phone || '' !== $details_box_width ) {
			$details_box_width_responsive_active = et_pb_get_responsive_status( $details_box_width_last_edited );

			$details_box_width_values = array(
				'desktop' => $details_box_width,
				'tablet'  => $details_box_width_responsive_active ? $details_box_width_tablet : '',
				'phone'   => $details_box_width_responsive_active ? $details_box_width_phone : '',
			);

			et_pb_generate_responsive_css( $details_box_width_values, '%%order_class%% .hw_se_box_three', 'flex-basis', $function_name );
		}
		if ( '' !== $date_image_box_order ) {
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .hw_se_box_one',
				'declaration' => sprintf(
					'order: %1$s;',
					esc_html( $date_image_box_order )
				),
			) );
		}
		if ( '' !== $content_box_order ) {
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .hw_se_box_two',
				'declaration' => sprintf(
					'order: %1$s;',
					esc_html( $content_box_order )
				),
			) );
		}
		if ( '' !== $details_box_order ) {
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .hw_se_box_three',
				'declaration' => sprintf(
					'order: %1$s;',
					esc_html( $details_box_order )
				),
			) );
		}
/*		if ( '' !== $content_box_width ) {
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .hw_se_box_two',
				'declaration' => sprintf(
					'flex-basis: %1$s;',
					esc_html( $content_box_width )
				),
			) );
		}
		if ( '' !== $details_box_width ) {
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .hw_se_box_three',
				'declaration' => sprintf(
					'flex-basis: %1$s;',
					esc_html( $details_box_width )
				),
			) );
		}
		if ( '' !== $date_width ) {
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .hw_se_date_box',
				'declaration' => sprintf(
					'min-width: %1$s;
					max-width: %1$s;',
					esc_html( $date_width )
				),
			) );

			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .hw_se_buttons',
				'declaration' => sprintf(
					'padding-left: %1$s;',
					esc_html( $date_width )
				),
			) );

		}
*/		if ( '' !== $date_height ) {
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .hw_se_date',
				'declaration' => sprintf(
					'min-height: %1$s;',
					esc_html( $date_height )
				),
			) );
/*			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .hw_se_date',
				'declaration' => sprintf(
					'transform: translateZ(calc(%1$s / 2));',
					esc_html( $date_height )
				),
			) );
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .hw_se_image',
				'declaration' => sprintf(
					'transform: rotateX(-90deg) translateZ(calc(-%1$s / 2));',
					esc_html( $date_height )
				),
			) );
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .hw_se_box_two, %%order_class%% .hw_se_box_three',
				'declaration' => sprintf(
					'min-height: calc(%1$s / 2);',
					esc_html( $date_height )
				),
			) );
*/		}

		if ( 'off' !== $date_inline ) {
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .hw_se_day, %%order_class%% .hw_se_month',
				'declaration' => sprintf(
					'flex-basis: 100%%;'
				),
			) );
		}
		if ( 'off' !== $use_date_background_color && '' !== $date_background_color  ) {
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .hw_se_date',
				'declaration' => sprintf(
					'background-color: %1$s;',
					esc_html( $date_background_color )
				),
			) );
		}
		if ( 'off' !== $use_background_color && '' !== $background_color  ) {
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%%',
				'declaration' => sprintf(
					'background-color: %1$s;',
					esc_html( $background_color )
				),
			) );
		}

		// set the correct default value for $transparent_background option if plugin activated.
		if ( et_is_builder_plugin_active() && 'default' === $use_background_color ) {
			$use_background_color = '' !== $background_color ? 'off' : 'on';
		} elseif ( 'default' === $use_background_color ) {
			$use_background_color = 'off';
		}

		if ( '' !== $background_color && 'off' === $use_background_color ) {
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%%',
				'declaration' => sprintf(
					'background-color:%s !important;',
					esc_attr( $background_color )
				),
			) );
		}
		if ( '' !== $button_one_url && '' !== $button_one_text ) {
			$button_one_text = sprintf( '<a class="hw_button_one et_pb_button%4$s%5$s" itemprop="url" href="%1$s"%3$s>%2$s</a>',
				esc_url( $button_one_url ),
				esc_html( $button_one_text ),
				'' !== $custom_icon ? sprintf(
					' data-icon="%1$s"',
					esc_attr( et_pb_process_font_icon( $custom_icon ) )
				) : '',
				'' !== $custom_icon ? ' et_pb_custom_button_icon' : '',
				esc_attr( $class )
			);
		}
		if ( '' !== $button_two_url && '' !== $button_two_text ) {
			$button_two_text = sprintf( '<a class="hw_button_two et_pb_button%4$s%5$s" href="%1$s"%3$s>%2$s</a>',
				esc_url( $button_two_url ),
				esc_html( $button_two_text ),
				'' !== $button_two_icon && 'on' === $custom_button_two ? sprintf(
					' data-icon="%1$s"',
					esc_attr( et_pb_process_font_icon( $button_two_icon ) )
				) : '',
				'' !== $button_two_icon && 'on' === $custom_button_two ? ' et_pb_custom_button_icon' : '',
				esc_attr( $class )
			);
		}

/*		
		$button_one_icon                = $this->shortcode_atts['button_one_icon'];
		$custom_button_one              = $this->shortcode_atts['custom_button_one'];
		$button_two_icon                = $this->shortcode_atts['button_two_icon'];
		$custom_button_two              = $this->shortcode_atts['custom_button_two'];



if ( '' !== $button_one_text ) {
			$button_output .= sprintf(
				'<a href="%2$s" class="et_pb_more_button et_pb_button et_pb_button_one%4$s"%3$s%5$s>%1$s</a>',
				( '' !== $button_one_text ? esc_attr( $button_one_text ) : '' ),
				( '' !== $button_one_url ? esc_url( $button_one_url ) : '#' ),
				'' !== $custom_icon_1 && 'on' === $button_custom_1 ? sprintf(
					' data-icon="%1$s"',
					esc_attr( et_pb_process_font_icon( $custom_icon_1 ) )
				) : '',
				'' !== $custom_icon_1 && 'on' === $button_custom_1 ? ' et_pb_custom_button_icon' : '',
				$this->get_rel_attributes( $button_one_rel )
			);
		}

		if ( '' !== $button_two_text ) {
			$button_output .= sprintf(
				'<a href="%2$s" class="et_pb_more_button et_pb_button et_pb_button_two%4$s"%3$s%5$s>%1$s</a>',
				( '' !== $button_two_text ? esc_attr( $button_two_text ) : '' ),
				( '' !== $button_two_url ? esc_url( $button_two_url ) : '#' ),
				'' !== $custom_icon_2 && 'on' === $button_custom_2 ? sprintf(
					' data-icon="%1$s"',
					esc_attr( et_pb_process_font_icon( $custom_icon_2 ) )
				) : '',
				'' !== $custom_icon_2 && 'on' === $button_custom_2 ? ' et_pb_custom_button_icon' : '',
				$this->get_rel_attributes( $button_two_rel )
			);
		}
		span.street:after {
    content: ',';
	
				'show_venue_address',
			'event_address_line_1',
			'event_address_town',
			'event_address_city',
			'event_address_county',
			'event_address_state',
			'event_address_zip',
			'event_address_country',

}
		*/

// ADDRESS Seperators

		if ( 'off' !== $show_venue_address ) {
			if ( '' !== $event_address_line_1 ) {
				ET_Builder_Element::set_style( $function_name, array(
					'selector'    => '%%order_class%% span.hw_se_street:after',
					'declaration' => 'content: ","'
				) );
			}
			if ( '' !== $event_address_town ) {
				ET_Builder_Element::set_style( $function_name, array(
					'selector'    => '%%order_class%% span.hw_se_town:after',
					'declaration' => 'content: ","'
				) );
			}
			if ( '' !== $event_address_city ) {
				ET_Builder_Element::set_style( $function_name, array(
					'selector'    => '%%order_class%% span.hw_se_city:after',
					'declaration' => 'content: ","'
				) );
			}
			if ( '' !== $event_address_county ) {
				ET_Builder_Element::set_style( $function_name, array(
					'selector'    => '%%order_class%% span.hw_se_county:after',
					'declaration' => 'content: ","'
				) );
			}
			if ( '' !== $event_address_state ) {
				ET_Builder_Element::set_style( $function_name, array(
					'selector'    => '%%order_class%% span.hw_se_region-state:after',
					'declaration' => 'content: "."'
				) );
			}
			if ( '' !== $event_address_country ) {
				ET_Builder_Element::set_style( $function_name, array(
					'selector'    => '%%order_class%% span.hw_se_country:after',
					'declaration' => 'content: "."'
				) );
			}
		}

		if ( '' !== $event_image ) { 
			$event_image_main = sprintf(
				'<a href="%1$s" class="hw_se_image et_pb_lightbox_image" %3$s><img itemprop="image" src="%1$s" alt="%2$s"%3$s /></a>',
				esc_url( $event_image ),
				esc_attr( $event_image_alt ),
				( '' !== $event_image_title_text ? sprintf( ' title="%1$s"', esc_attr( $event_image_title_text ) ) : '' )
			);
		}
		if ( 'off' !== $use_date_background_image && '' !== $event_image ) { 
			ET_Builder_Element::set_style( $function_name, array(
				'selector'    => '%%order_class%% .hw_se_date:after',
				'declaration' => sprintf(
					'content:"";
					background: url(%1$s);
					opacity: %2$s;
					background-position:%3$s;
					background-size:%4$s;
					top: 0;
					left: 1px;
					bottom: 0;
					right: 0;
					position: absolute;
					z-index: -1; ',
					esc_html( $event_image ),
					esc_html( $date_background_image_opacity ),
					esc_html( $date_background_position ),
					esc_html( $date_background_size )
				),
			) );
		}

		$output = sprintf(
			'<div%9$s class="hw_se%10$s et_pb_hw_se" itemscope itemtype="http://schema.org/Event">

				<div class="hw_se_box_container">
					<div class="hw_se_box_inner">
						<div class="hw_se_box_one">
							<div class="hw_se_date_box">
								<div class="date_box hw_se_date">
									%3$s%4$s%5$s
								</div><!-- date -->
								%13$s
							</div><!-- box -->
						</div><!-- hw_se_box_one -->
						<div class="hw_se_box_two">
							%1$s
							%17$s
						</div>
						<div class="hw_se_box_three">
							%2$s
							<div class="hw_se_event_details">
							%15$s%6$s%16$s
							</div>
							%11$s
							<div class="hw_se_contact_info">
							<span class="contact-title">Contact Info</span>
							%14$s
							</div>
						</div>
					</div>
				<div class="hw_se_buttons">
					%8$s%12$s
				</div>
				</div><!-- hw_se_box_container -->
			</div>',
			$event_title,
			( '' !== $event_location ? sprintf( '<h2 class="header">Event Details</h2>', esc_html( $event_location ) ) : '' ),
			( '' !== $day ? sprintf( '<div class="hw_se_day">%1$s%2$s</div>', esc_html( $day ), 
				'off' !== $day_suffix ? sprintf( '<span class="day_suffix">%1$s</span>', esc_html( $suffix ) ) : '' ) : '' ),
			( '' !== $month ? sprintf( '<div class="hw_se_month">%1$s</div>', esc_html( $month ) ) : '' ),
			( '' !== $year ? sprintf( '<div class="hw_se_year">%1$s</div>', esc_html( $year ) ) : '' ),
			( 'off' !== $show_time && '' !== $time ? sprintf( '<p><time datetime="%4$s-%5$s-%6$s" itemprop="startDate" class="hw_se_time">%2$s %1$s %3$s</time></p>', 
				esc_html( $time ), 
				$time_prefix, 
				$time_suffix,
				$year,
				$month_num,
				$day ) : '' ),
			$button_one_url,
			$button_one_text,
			( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
			( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' ),
			( 'off' !== $show_venue_address ? sprintf( '<div class="hw_se_venue_address" itemprop="location" itemscope itemtype="http://schema.org/Place">
				%1$s
				<div class="address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
					%2$s
					%3$s
					%4$s
					%5$s
					%6$s
					%7$s
					%8$s
				</div>
			</div>', 
				 '' !== $event_location ? sprintf( '<span class="location-title" itemprop="name">%1$s</span>', esc_html( $event_location ) ) : '', 
				 '' !== $event_address_line_1 ? sprintf( '<span class="hw_se_street" itemprop="streetAddress">%1$s</span>', esc_html( $event_address_line_1 ) ) : '', 
				 '' !== $event_address_town ? sprintf( '<span class="hw_se_town">%1$s</span>', esc_html( $event_address_town ) ) : '', 
				 '' !== $event_address_city ? sprintf( '<span class="hw_se_city" itemprop="addressLocality">%1$s</span>', esc_html( $event_address_city ) ) : '', 
				 '' !== $event_address_county ? sprintf( '<span class="hw_se_county">%1$s</span>', esc_html( $event_address_county ) ) : '', 
				 '' !== $event_address_state ? sprintf( '<span class="hw_se_region-state" itemprop="addressRegion">%1$s</span>', esc_html( $event_address_state ) ) : '', 
				 '' !== $event_address_zip ? sprintf( '<span itemprop="postalCode"><a href="%2$s">%1$s</a></span>', esc_html( $event_address_zip ), $button_one_url ) : '', 
				 '' !== $event_address_country ? sprintf( '<span class="hw_se_country" itemprop="addressCountry">%1$s</span>', esc_html( $event_address_country ) ) : '' ) : '' ),
			$button_two_text,
			$event_image_main,
			( 'off' !== $show_venue_address && '' !== $venue_telephone ? sprintf( '<span class="hw_venue_telephone">Telephone:<a href="tel:%1$s"> %1$s</a></span>', esc_attr( $venue_telephone ) ) : '' ),
			( 'off' !== $show_time && '' !== $time_before ? sprintf( '<span class="hw_se_time_before">%1$s</span>', esc_html( $time_before ) ) : '' ),
			( 'off' !== $show_time && '' !== $time_after ? sprintf( '<span class="hw_se_time_after">%1$s</span>', esc_html( $time_after ) ) : '' ),
			( 'off' !== $show_content ? sprintf( '<div class="hw_se_content" itemprop="description">%1$s</div>', $this->shortcode_content ) : '' )
			 
		);

		return $output;
	}
}
new ET_Builder_Module_HW_SE;
