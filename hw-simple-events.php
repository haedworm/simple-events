<?php
/*
Plugin Name: Simple Events Module
Plugin URI:  https://hædworm.com/project/simple-events-module/
Description: An Custom Event Module for Divi
Version:     1.0.0
Author:      Hædworm a.k.a Darren Woodley
Author URI:  https://hædworm.com 
Text Domain: hw_simple_events
*/

define( 'HW_SE_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
	
/* Enqueues */
function hw_se_enqueue() {
    wp_enqueue_style('hw-simple-events-image-styles', HW_SE_PLUGIN_URL . 'assets/css/styles.css');
}
add_action( 'wp_enqueue_scripts', 'hw_se_enqueue' );
	  
/* Enqueue Admin Styles  */
function hw_se_admin() {
    wp_enqueue_style('hw-simple-events-image-admin-styles', HW_SE_PLUGIN_URL . 'admin/css/admin.css');
    wp_enqueue_script( 'change-title', HW_SE_PLUGIN_URL . 'admin/js/admin.js');
}
add_action('admin_enqueue_scripts', 'hw_se_admin');

/* Module  */
function hw_se_setup(){
	if(class_exists("ET_Builder_Module")){ 
		include( plugin_dir_path( __FILE__ ) . 'assets/modules/module.php');
	}
}
add_action('et_builder_ready', 'hw_se_setup');

/* Tabs */
add_filter('et_builder_main_tabs', 'hw_se_tabs' );

if ( ! function_exists( 'hw_se_tabs' ) ) :
function hw_se_tabs($tabs) {
    $tabs['hw_se_date'] = esc_html__( 'Time/Date', 'et_builder' );
    $tabs['hw_se_location'] = esc_html__( 'Location', 'et_builder' );
    return $tabs;
}
endif;

add_filter('et_pb_new_layout_module_tabs', 'hw_se_module_tabs' );

if ( ! function_exists( 'hw_se_module_tabs' ) ) :
function hw_se_module_tabs($tabs) {
    $tabs['hw_se_date']		= esc_html__( 'Event Settings', 'et_builder' );
    $tabs['hw_se_location'] = esc_html__( 'Event Location', 'et_builder' );
    return $tabs;
}
endif;