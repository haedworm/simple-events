# README #

* Simple Event Module for Divi

* A simple Event Module to add events to Divi without the need for large or complicated plugins.
* Includes fields for artist/speaker, location/venue. Date-picker with full range of configuration options for diaplying Day/Month/Year and Time.
* Two button fields for adding Ticket and Read More links. Image field with animation option.
* Link to Google Maps from Post Code. Telephone link for Venue/Location
* Event Schema Data for SEO.
* The Module automatically displays the Event Name and Date in the Admin Title area so you can easily re-order events in the Builder.

* To use the plugin install like any other WordPress plugin then find the new module in the builder in the Standard Sections.

* You will need the Premium Divi Theme to use this plugin. 

### Who do I talk to? ###

* xn-hdwrom--pua.com
* Hædworm